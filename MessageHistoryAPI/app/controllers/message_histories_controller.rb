class MessageHistoriesController < ApplicationController
	
  before_action :set_message_history, only: [:show, :update, :destroy]

  # GET /message_histories
  # GET /message_histories.json
  def index
    @message_histories = MessageHistory.all

    render json: @message_histories
  end

  # GET /message_histories/1
  # GET /message_histories/1.json
  def show
    render json: @message_history
  end

  # POST /message_histories
  # POST /message_histories.json
    def create
  
	#for message_history in @message_history do
     #   new_visit = MessageHistory.new(message_history_params)
     #   new_visit.save
    #end
	#for message_history in MessageHistory.new(message_history_params) do
		@message_history = MessageHistory.new(message_history_params)

		if @message_history.save
		 render json: @message_history, status: :created, location: @message_history
		else
		  render json: @message_history.errors, status: :unprocessable_entity
		end
	#end
  end
  
 
  
  

  # PATCH/PUT /message_histories/1
  # PATCH/PUT /message_histories/1.json
  def update
    @message_history = MessageHistory.find(params[:id])

    if @message_history.update(message_history_params)
      head :no_content
    else
      render json: @message_history.errors, status: :unprocessable_entity
    end
  end

  # DELETE /message_histories/1
  # DELETE /message_histories/1.json
  def destroy
    @message_history.destroy

    head :no_content
  end

  private

    def set_message_history
      @message_history = MessageHistory.find(params[:id])
    end

    def message_history_params
		#params.permit(message_history: [])
      params.require(:message_history).permit(:message_history_id, :message_id, :my_number, :other_number, :message_type, :message_length, :happened_at, :status, :message_log_id, :message_text)
      #params.permit(message_history:[ :messageHistoryId, :message_id, :myNumber, :otherNumber, :message_type, :messageLength, :happenedAt, :status, :messageLogId, :messageText])
		
	end
end
