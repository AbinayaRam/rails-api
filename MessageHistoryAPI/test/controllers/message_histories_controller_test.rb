require 'test_helper'

class MessageHistoriesControllerTest < ActionController::TestCase
  setup do
    @message_history = message_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:message_histories)
  end

  test "should create message_history" do
    assert_difference('MessageHistory.count') do
      post :create, message_history: { happenedAt: @message_history.happenedAt, messageHistoryId: @message_history.messageHistoryId, messageLength: @message_history.messageLength, messageLogId: @message_history.messageLogId, messageText: @message_history.messageText, message_id: @message_history.message_id, message_type: @message_history.message_type, myNumber: @message_history.myNumber, otherNumber: @message_history.otherNumber, status: @message_history.status }
    end

    assert_response 201
  end

  test "should show message_history" do
    get :show, id: @message_history
    assert_response :success
  end

  test "should update message_history" do
    put :update, id: @message_history, message_history: { happenedAt: @message_history.happenedAt, messageHistoryId: @message_history.messageHistoryId, messageLength: @message_history.messageLength, messageLogId: @message_history.messageLogId, messageText: @message_history.messageText, message_id: @message_history.message_id, message_type: @message_history.message_type, myNumber: @message_history.myNumber, otherNumber: @message_history.otherNumber, status: @message_history.status }
    assert_response 204
  end

  test "should destroy message_history" do
    assert_difference('MessageHistory.count', -1) do
      delete :destroy, id: @message_history
    end

    assert_response 204
  end
end
