class RenameColumnXinTableYtoZ < ActiveRecord::Migration
  def change
	rename_column :message_histories, :messageHistoryId, :message_history_id
	rename_column :message_histories, :myNumber, :my_number
	rename_column :message_histories, :otherNumber, :other_number
	
  end
end
