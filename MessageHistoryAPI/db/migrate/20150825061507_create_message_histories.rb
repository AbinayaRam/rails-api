class CreateMessageHistories < ActiveRecord::Migration
  def change
    create_table :message_histories do |t|
      t.integer :messageHistoryId
      t.text :message_id
      t.text :myNumber
      t.text :otherNumber
      t.text :message_type
      t.integer :messageLength
      t.datetime :happenedAt
      t.text :status
      t.integer :messageLogId
      t.text :messageText

      t.timestamps
    end
  end
end
