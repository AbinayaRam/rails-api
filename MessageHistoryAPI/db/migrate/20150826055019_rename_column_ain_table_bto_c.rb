class RenameColumnAinTableBtoC < ActiveRecord::Migration
  def change
	rename_column :message_histories, :messageLength, :message_length
	rename_column :message_histories, :happenedAt, :happened_at
	rename_column :message_histories, :messageLogId, :message_log_id
	rename_column :message_histories, :messageText, :message_text
  end
end
