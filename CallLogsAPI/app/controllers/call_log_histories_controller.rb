class CallLogHistoriesController < ApplicationController
  before_action :set_call_log_history, only: [:show, :update, :destroy]

  # GET /call_log_histories
  # GET /call_log_histories.json
  def index
    @call_log_histories = CallLogHistory.all

    render json: @call_log_histories
  end

  # GET /call_log_histories/1
  # GET /call_log_histories/1.json
  def show
    render json: @call_log_history
  end

  # POST /call_log_histories
  # POST /call_log_histories.json
  def create
    @call_log_history = CallLogHistory.new(call_log_history_params)

    if @call_log_history.save
      render json: @call_log_history, status: :created, location: @call_log_history
    else
      render json: @call_log_history.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /call_log_histories/1
  # PATCH/PUT /call_log_histories/1.json
  def update
    @call_log_history = CallLogHistory.find(params[:id])

    if @call_log_history.update(call_log_history_params)
      head :no_content
    else
      render json: @call_log_history.errors, status: :unprocessable_entity
    end
  end

  # DELETE /call_log_histories/1
  # DELETE /call_log_histories/1.json
  def destroy
    @call_log_history.destroy

    head :no_content
  end

  private

    def set_call_log_history
      @call_log_history = CallLogHistory.find(params[:id])
    end

    def call_log_history_params
      params.require(:call_log_history).permit(:call_history_id, :local_id, :my_number, :other_number, :call_type, :seconds, :happened_at, :status, :log_id, :phone_account_id, :sim1_roaming, :sim2_roaming)
    end
end
