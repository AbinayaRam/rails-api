require 'test_helper'

class CallLogHistoriesControllerTest < ActionController::TestCase
  setup do
    @call_log_history = call_log_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:call_log_histories)
  end

  test "should create call_log_history" do
    assert_difference('CallLogHistory.count') do
      post :create, call_log_history: { call_history_id: @call_log_history.call_history_id, call_type: @call_log_history.call_type, happened_at: @call_log_history.happened_at, local_id: @call_log_history.local_id, log_id: @call_log_history.log_id, my_number: @call_log_history.my_number, other_number: @call_log_history.other_number, phone_account_id: @call_log_history.phone_account_id, seconds: @call_log_history.seconds, sim1_roaming: @call_log_history.sim1_roaming, sim2_roaming: @call_log_history.sim2_roaming, status: @call_log_history.status }
    end

    assert_response 201
  end

  test "should show call_log_history" do
    get :show, id: @call_log_history
    assert_response :success
  end

  test "should update call_log_history" do
    put :update, id: @call_log_history, call_log_history: { call_history_id: @call_log_history.call_history_id, call_type: @call_log_history.call_type, happened_at: @call_log_history.happened_at, local_id: @call_log_history.local_id, log_id: @call_log_history.log_id, my_number: @call_log_history.my_number, other_number: @call_log_history.other_number, phone_account_id: @call_log_history.phone_account_id, seconds: @call_log_history.seconds, sim1_roaming: @call_log_history.sim1_roaming, sim2_roaming: @call_log_history.sim2_roaming, status: @call_log_history.status }
    assert_response 204
  end

  test "should destroy call_log_history" do
    assert_difference('CallLogHistory.count', -1) do
      delete :destroy, id: @call_log_history
    end

    assert_response 204
  end
end
