class CreateCallLogHistories < ActiveRecord::Migration
  def change
    create_table :call_log_histories do |t|
      t.integer :call_history_id
      t.text :local_id
      t.text :my_number
      t.text :other_number
      t.text :call_type
      t.integer :seconds
      t.datetime :happened_at
      t.text :status
      t.integer :log_id
      t.text :phone_account_id
      t.text :sim1_roaming
      t.text :sim2_roaming

      t.timestamps
    end
  end
end
