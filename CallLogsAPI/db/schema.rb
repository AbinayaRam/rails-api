# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150824125322) do

  create_table "call_log_histories", force: true do |t|
    t.integer  "call_history_id"
    t.text     "local_id"
    t.text     "my_number"
    t.text     "other_number"
    t.text     "call_type"
    t.integer  "seconds"
    t.datetime "happened_at"
    t.text     "status"
    t.integer  "log_id"
    t.text     "phone_account_id"
    t.text     "sim1_roaming"
    t.text     "sim2_roaming"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
