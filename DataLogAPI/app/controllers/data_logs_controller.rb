class DataLogsController < ApplicationController
  before_action :set_data_log, only: [:show, :update, :destroy]

  # GET /data_logs
  # GET /data_logs.json
  def index
    @data_logs = DataLog.all

    render json: @data_logs
  end

  # GET /data_logs/1
  # GET /data_logs/1.json
  def show
    render json: @data_log
  end

  # POST /data_logs
  # POST /data_logs.json
  def create
    @data_log = DataLog.new(data_log_params)

    if @data_log.save
      render json: @data_log, status: :created, location: @data_log
    else
      render json: @data_log.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /data_logs/1
  # PATCH/PUT /data_logs/1.json
  def update
    @data_log = DataLog.find(params[:id])

    if @data_log.update(data_log_params)
      head :no_content
    else
      render json: @data_log.errors, status: :unprocessable_entity
    end
  end

  # DELETE /data_logs/1
  # DELETE /data_logs/1.json
  def destroy
    @data_log.destroy

    head :no_content
  end

  private

    def set_data_log
      @data_log = DataLog.find(params[:id])
    end

    def data_log_params
      params.require(:data_log).permit(:data_log_id, :data_id, :data_rx, :data_tx, :data_type, :happened_at, :status, :myNumber)
    end
end
