class CreateDataLogs < ActiveRecord::Migration
  def change
    create_table :data_logs do |t|
      t.integer :data_log_id
      t.text :data_id
      t.decimal :data_rx
      t.decimal :data_tx
      t.text :data_type
      t.datetime :happened_at
      t.text :status
      t.text :myNumber

      t.timestamps
    end
  end
end
