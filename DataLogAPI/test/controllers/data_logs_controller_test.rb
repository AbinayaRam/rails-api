require 'test_helper'

class DataLogsControllerTest < ActionController::TestCase
  setup do
    @data_log = data_logs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:data_logs)
  end

  test "should create data_log" do
    assert_difference('DataLog.count') do
      post :create, data_log: { data_id: @data_log.data_id, data_log_id: @data_log.data_log_id, data_rx: @data_log.data_rx, data_tx: @data_log.data_tx, data_type: @data_log.data_type, happened_at: @data_log.happened_at, myNumber: @data_log.myNumber, status: @data_log.status }
    end

    assert_response 201
  end

  test "should show data_log" do
    get :show, id: @data_log
    assert_response :success
  end

  test "should update data_log" do
    put :update, id: @data_log, data_log: { data_id: @data_log.data_id, data_log_id: @data_log.data_log_id, data_rx: @data_log.data_rx, data_tx: @data_log.data_tx, data_type: @data_log.data_type, happened_at: @data_log.happened_at, myNumber: @data_log.myNumber, status: @data_log.status }
    assert_response 204
  end

  test "should destroy data_log" do
    assert_difference('DataLog.count', -1) do
      delete :destroy, id: @data_log
    end

    assert_response 204
  end
end
